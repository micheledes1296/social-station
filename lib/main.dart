import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logs/logs.dart';
import 'package:provider/provider.dart';

import 'app.dart';

final Log httpLog = new Log('http');

void main() async {
  httpLog.enabled = true;

  Provider.debugCheckInvalidValueType = null;

  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(App());
}
