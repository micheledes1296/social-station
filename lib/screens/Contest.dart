import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:SocialStation/widgets/EventTile.dart';
import 'package:SocialStation/services/wordpress.dart';
import 'dart:async';
import 'package:SocialStation/models/blog_news.dart';
import 'package:provider/provider.dart';
import 'package:async/src/async_memoizer.dart';
import 'package:SocialStation/models/app.dart';
import 'package:SocialStation/widgets/blog/detailed_blog/blog_view.dart';


class Contest extends StatefulWidget {
  final config;

  Contest({this.config});



  @override
  _ContestState createState() {
    // TODO: implement createState
    return _ContestState();
  }
}

class _ContestState extends State<Contest> {


  final WordPress _service = WordPress();
  Future<List<BlogNews>> _getBlogsLayout;

  final _memoizer = AsyncMemoizer<List<BlogNews>>();

  @override
  void initState() {
    // only create the future once
    new Future.delayed(Duration.zero, () {
      setState(() {
        _getBlogsLayout = getBlogLayout(context);
      });
    });


    super.initState();
  }

  Future<List<BlogNews>> getBlogLayout(context) => _memoizer.runOnce(
        () => _service.fetchBlogLayout(
        config: widget.config, lang: Provider.of<AppModel>(context).locale),
  );


  @override
  Widget build(BuildContext context) {

    List<dynamic> events = widget.config["elements"];
    return FutureBuilder(
        future: _getBlogsLayout,

        builder: (context, snapshot) {
          if(snapshot.connectionState != ConnectionState.done){
            return Center(child: CircularProgressIndicator());

          } else if( snapshot.connectionState == ConnectionState.done) {
            return  Padding(
              padding: const EdgeInsets.only(left:8,right: 8,top: 8),
              child: ListView.separated(
                  itemBuilder: ( _ , index )=> GestureDetector(
                    child: EventTile(config: events[index],title: snapshot.data.reversed.toList()[index].title),
                    onTap: () {
                      if (snapshot.hasData)
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => getDetailBlog(snapshot.data.reversed.toList()[index]),
                          ),
                        );
                    },
                  ),
                  separatorBuilder:(_,index) => SizedBox(height: 20),
                  itemCount: getLenght(snapshot.data)
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.only(left:8,right: 8,top: 8),
              child: ListView.separated(
                  itemBuilder: ( _ , index )=> GestureDetector(
                    child: Container(color: Colors.grey,),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => getDetailBlog(snapshot.data[index]),
                        ),
                      );
                    },
                  ),
                  separatorBuilder:(_,index) => SizedBox(height: 20),
                  itemCount: events.length-2
              ),
            );
          }
        }
    );
  }

}