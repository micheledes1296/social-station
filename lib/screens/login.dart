import 'package:SocialStation/widgets/blog/logo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../common/config.dart';
import '../generated/i18n.dart';
import '../models/user.dart';
import 'registration.dart';

class LoginScreen extends StatefulWidget {
  final bool fromCart;

  LoginScreen({this.fromCart = false});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginScreen> {
  String username, password;
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  var parentContext;
  final _auth = FirebaseAuth.instance;

  void _welcomeMessage(user, context) {
    if (widget.fromCart) {
      Navigator.of(context).pop(user);
    } else {
      final snackBar =
          SnackBar(content: Text(S.of(context).welcome + ' ${user.name} !'));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  void _failMessage(message, context) {
    /// Showing Error messageSnackBarDemo
    /// Ability so close message
    final snackBar = SnackBar(
      content: Text('Warning: $message'),
      duration: Duration(seconds: 3),
      action: SnackBarAction(
        label: S.of(context).close,
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    Scaffold.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    parentContext = context;

    String forgetPasswordUrl = serverConfig['forgetPassword'];

    Future launchForgetPasswordURL(String url) async {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: true, forceWebView: true);
      } else {}
    }

    Future launcSignUpURL() async {

      String url = "https://www.socialstation.it/register";
      if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false , forceWebView: false);
      } else {}
    }

    _login(context) async {
      if (username == null || password == null) {
        var snackBar = SnackBar(content: Text(S.of(context).pleaseInput));
        Scaffold.of(context).showSnackBar(snackBar);
      } else {
        print(password);
        Provider.of<UserModel>(context).login(
          username: username.trim(),
          password: password.trim(),
          success: (user) {
            _welcomeMessage(user, context);
            _auth
                .signInWithEmailAndPassword(email: username, password: password)
                .catchError((onError) {
              if (onError.code == 'ERROR_USER_NOT_FOUND')
                _auth
                    .createUserWithEmailAndPassword(
                        email: username, password: password)
                    .then((_) {
                  _auth.signInWithEmailAndPassword(
                      email: username, password: password);
                });
            });
          },
          fail: (message) => _failMessage(message, context),
        );
      }
    }

    _loginFacebook(context) async {
      Provider.of<UserModel>(context).loginFB(
        success: (user) => _welcomeMessage(user, context),
        fail: (message) => _failMessage(message, context),
      );
    }

    _loginSMS(context) async {
      Provider.of<UserModel>(context).loginSMS(
        success: (user) => _welcomeMessage(user, context),
        fail: (message) => _failMessage(message, context),
      );
    }

    return  SafeArea(
      child: Builder(
        builder: (context) => Stack(children: [
          ListenableProvider.value (
            value: Provider.of<UserModel>(context),
            child: Consumer<UserModel>(builder: (context, model, child) {
              return SingleChildScrollView(

                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Column(
                  children: <Widget>[
                    const SizedBox(height: 50.0),
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                child: Logo(size: 30)
                            ),
                          ],
                        ),
                      ],
                    ),

                    const SizedBox(height: 120.0),

                    TextField(
                        controller: _usernameController,
                        onChanged: (value) => username = value,
                        decoration: const InputDecoration(
                          labelText: 'Username',
                        )),

                    const SizedBox(height: 12.0),
                    Stack(children: <Widget>[
                      TextField(
                        onChanged: (value) => password = value,
                        obscureText: true,
                        controller: _passwordController,
                        decoration: const InputDecoration(
                          labelText: 'Password',
                        ),
                      ),
                      Positioned(
                        right: 4,
                        bottom: 20,
                        child: GestureDetector(
                          child: Text(
                            " " + S.of(context).reset,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ),
                          onTap: () {
                            launchForgetPasswordURL(forgetPasswordUrl);
                          },
                        ),
                      )
                    ]),
                    SizedBox(
                      height: 60.0,
                    ),
                    SignInButtonBuilder(
                      text: S.of(context).signInWithEmail,
                      icon: Icons.email,
                      onPressed: () {
                        _login(context);
                      },
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      elevation: 0,
                      backgroundColor: Theme.of(context).primaryColor,
                      width: 150,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),

                    Stack(
                        alignment: AlignmentDirectional.center,
                        children: <Widget>[
                          SizedBox(
                              height: 60.0,
                              width: 200.0,
                              child: Divider(color: Colors.grey.shade300)),
                          Container(
                              height: 30, width: 40, color: Colors.white),
                          Text(S.of(context).or,
                              style: TextStyle(
                                  fontSize: 12, color: Colors.grey.shade400))
                        ]),


                    // ACCOUNTKIT FACEBOOK SMS LOGIN

                    /*Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RawMaterialButton(
                          onPressed: () => _loginFacebook(context),
                          child: Icon(
                            FontAwesomeIcons.facebookF,
                            color: Color(0xFF4267B2),
                            size: 24.0,
                          ),
                          shape: CircleBorder(),
                          elevation: 0.4,
                          fillColor: Colors.grey.shade50,
                          padding: const EdgeInsets.all(15.0),
                        ),
                        RawMaterialButton(
                          onPressed: () => _loginSMS(context),
                          child: Icon(
                            FontAwesomeIcons.sms,
                            color: Colors.lightBlue,
                            size: 24.0,
                          ),
                          shape: CircleBorder(),
                          elevation: 0.4,
                          fillColor: Colors.grey.shade50,
                          padding: const EdgeInsets.all(15.0),
                        ),
                      ],
                    ),*/

                    SizedBox(
                      height: 30.0,
                    ),
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(S.of(context).dontHaveAccount),
                            GestureDetector(
                              onTap: () {
                                launcSignUpURL();
                              },
                              child: Text(" " + S.of(context).signup,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).primaryColor)),
                            ),
                          ],
                        ),
                      ],
                    )

//            FlatButton(
//              child: const Text('CANCEL'),
//              shape: const BeveledRectangleBorder(
//                borderRadius: BorderRadius.all(Radius.circular(7.0)),
//              ),
//              onPressed: () {
//                _usernameController.clear();
//                _passwordController.clear();
//                Navigator.pop(context);
//              },
//            ),
                  ],
                ),
              );
            }),
          ),
//            if (widget.fromCart)
//              Positioned(
//                  left: 10,
//                  child: IconButton(
//                      icon: Icon(
//                        Icons.close,
//                        color: Colors.black,
//                      ),
//                      onPressed: () {
//                        Navigator.of(context).pop();
//                      })),
        ]),
      ),
    );
  }
}

class PrimaryColorOverride extends StatelessWidget {
  const PrimaryColorOverride({Key key, this.color, this.child})
      : super(key: key);

  final Color color;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: child,
      data: Theme.of(context).copyWith(primaryColor: color),
    );
  }
}
