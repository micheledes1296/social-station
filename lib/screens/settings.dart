import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:notification_permissions/notification_permissions.dart';
import '../common/constants.dart';
import '../common/styles.dart';
import '../generated/i18n.dart';
import '../models/app.dart';
import '../models/user.dart';
import '../models/wishlist.dart';
import '../widgets/smartchat.dart';
import 'language.dart';
import 'notification.dart';
import 'package:flutter/cupertino.dart';
import 'package:SocialStation/screens/CreateNewPostPage.dart';
import 'package:rect_getter/rect_getter.dart';




import 'dart:io' show Platform;

class SettingScreen extends StatefulWidget {
  final User user;
  final VoidCallback onLogout;

  SettingScreen({this.user, this.onLogout});

  @override
  State<StatefulWidget> createState() {
    return SettingScreenState();
  }
}

class SettingScreenState extends State<SettingScreen>
    with TickerProviderStateMixin, WidgetsBindingObserver {

  GlobalKey rectGetterKey = RectGetter.createGlobalKey();
  Rect rect;
  final Duration animationDuration = Duration(milliseconds: 300);
  final Duration delay = Duration(milliseconds: 300);

  final bannerHigh = 200.0;
  bool enabledNotification = true;

  RateMyApp _rateMyApp = RateMyApp(
    minDays: 7,
    minLaunches: 10,
    remindDays: 7,
    remindLaunches: 10,
  );

  void checkNotificationPermission() async {
    try {
      NotificationPermissions.getNotificationPermissionStatus().then((status) {
        if (mounted)
          setState(() {
            enabledNotification = status == PermissionStatus.granted;
          });
      });
    } catch (err) {
//      print(err);
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      checkNotificationPermission();
    }
  }


  void _goToNextPage() {
    Navigator.of(context)
        .push(FadeRouteBuilder(page: FullPageEditorScreen() ))
        .then((_) => setState(() => rect = null));
  }

  @override
  Widget build(BuildContext context) {
    final wishListCount = Provider.of<WishListModel>(context).blogs.length;

    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          floatingActionButton: RectGetter(
            key: rectGetterKey,
            child: FloatingActionButton(
              onPressed: (){
                setState(() => rect = RectGetter.getRectFromKey(rectGetterKey));
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  setState(() =>
                  rect = rect.inflate(1.3 * MediaQuery.of(context).size.longestSide));
                  Future.delayed(animationDuration + delay, _goToNextPage);
                });
                Navigator.of(context).push(FadeRouteBuilder(page: FullPageEditorScreen()));
              },
              backgroundColor: kMainAppColor,
              elevation: 1,
              child: Icon(
                Icons.create,
                color: Colors.white,
              ),
            ),
          ),
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Theme.of(context).primaryColor,
                expandedHeight: bannerHigh,
                floating: true,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                    title: Text(S.of(context).settings,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black87,
                            fontWeight: FontWeight.w600)),
                    background: Image.network(
                      kProfileBackground,
                      fit: BoxFit.cover,
                    )),
              ),

              SliverList(
                delegate: SliverChildListDelegate(
                  <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 10.0,
                          ),
                          ListTile(
                            leading: widget.user.picture != null
                                ? CircleAvatar(
                                backgroundImage:
                                NetworkImage(widget.user.picture))
                                : Icon(Icons.face),
                            title: Text(widget.user.name,
                                style: TextStyle(fontSize: 16)),
                          ),
                          ListTile(
                            leading: Icon(Icons.email),
                            title: Text(widget.user.email,
                                style: TextStyle(fontSize: 16)),
                          ),
                          SizedBox(height: 30.0),
                          Text(S.of(context).generalSetting,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600)),
                          SizedBox(height: 10.0),

                          Divider(
                            color: Colors.black12,
                            height: 1.0,
                            indent: 75,
                            //endIndent: 20,
                          ),
                          Divider(
                            color: Colors.black12,
                            height: 1.0,
                            indent: 75,
                            //endIndent: 20,
                          ),
                          Card(
                            margin: EdgeInsets.only(bottom: 2.0),
                            elevation: 0,
                            child: SwitchListTile(
                              secondary: Icon(
                                Icons.dashboard,
                                color: Theme.of(context).accentColor,
                                size: 24,
                              ),
                              value: Provider.of<AppModel>(context).darkTheme,
                              activeColor: Color(0xFF0066B4),
                              onChanged: (bool value) {
                                if (value) {
                                  Provider.of<AppModel>(context).updateTheme(true);
                                } else
                                  Provider.of<AppModel>(context).updateTheme(false);
                              },
                              title: Text(
                                S.of(context).darkTheme,
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                          Divider(
                            color: Colors.black12,
                            height: 1.0,
                            indent: 75,
                            //endIndent: 20,
                          ),
                          Card(
                            margin: EdgeInsets.only(bottom: 2.0),
                            elevation: 0,
                            child: ListTile(
                              onTap: () {
                                _rateMyApp
                                    .showRateDialog(context)
                                    .then((v) => setState(() {}));
                              },
                              leading: Image.asset(
                                'assets/icons/profile/icon-star.png',
                                width: 24,
                                color: Theme.of(context).accentColor,
                              ),
                              title: Text(S.of(context).rateTheApp,
                                  style: TextStyle(fontSize: 16)),
                              trailing: Icon(Icons.arrow_forward_ios,
                                  size: 18, color: kGrey600),
                            ),
                          ),
                          Divider(
                            color: Colors.black12,
                            height: 1.0,
                            indent: 75,
                            //endIndent: 20,
                          ),

                          Card(
                            margin: EdgeInsets.only(bottom: 2.0),
                            elevation: 0,
                            child: ListTile(
                              onTap: widget.onLogout,
                              leading: Image.asset(
                                'assets/icons/profile/icon-logout.png',
                                width: 24,
                                color: Theme.of(context).accentColor,
                              ),
                              title: Text(S.of(context).logout,
                                  style: TextStyle(fontSize: 16)),
                              trailing: Icon(Icons.arrow_forward_ios,
                                  size: 18, color: kGrey600),
                            ),
                          ),
                          Divider(
                            color: Colors.black12,
                            height: 1.0,
                            indent: 75,
                            //endIndent: 20,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SliverPadding(
                padding: EdgeInsets.only(bottom: 150),
              )
            ],
          ),
        ),
        _ripple()
      ],
    );
  }

  Widget _ripple() {
    if (rect == null) {
      return Container();
    }
    return AnimatedPositioned(
      duration: Duration(milliseconds: 500),
      left: rect.left,
      right: MediaQuery.of(context).size.width - rect.right,
      top: rect.top,
      bottom: MediaQuery.of(context).size.height - rect.bottom,
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: kMainAppColor,
        ),
      ),
    );
  }
}




class FadeRouteBuilder<T> extends PageRouteBuilder<T> {
  final Widget page;

  FadeRouteBuilder({@required this.page})
      : super(
    pageBuilder: (context, animation1, animation2) => page,
    transitionsBuilder: (context, animation1, animation2, child) {
      return FadeTransition(opacity: animation1, child: child);
    },
  );
}