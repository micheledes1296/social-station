import 'package:SocialStation/widgets/ActiveIcon.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'common/config.dart' as config;
import 'common/constants.dart';
import 'generated/i18n.dart';
import 'models/category.dart';
import 'models/user.dart';
import 'screens/categories/index.dart';
import 'screens/search/search.dart';
import 'package:SocialStation/screens/home.dart';
import 'screens/user.dart';
import 'screens/wishlist.dart';
import 'models/blog_news.dart';
import 'package:flutter/cupertino.dart';

class MainTabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainTabsState();
  }
}

class MainTabsState extends State<MainTabs> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _auth = FirebaseAuth.instance;

  InterstitialAd interstitialAd;
  FirebaseUser loggedInUser;
  int pageIndex = 0;
  int currentPage = 0;
  String currentTitle = "Home";
  Color currentColor = Colors.deepPurple;
  bool isAdmin = false;
  var selectedIndex = 0;
  List<Widget> _tabView = [
    HomeScreen(),
    //CategoriesScreen(),
    SearchScreen(
//      isModal: false,
        ),
    WishList(),
    UserScreen()
  ];

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser();
      if (user != null) {
        setState(() {
          loggedInUser = user;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  List getChildren(List<Category> categories, Category category) {
    List<Widget> list = [];
    var children = categories.where((o) => o.parent == category.id).toList();
    if (children.length == 0) {
      list.add(
        ListTile(
          leading: Padding(
            child: Text(category.name),
            padding: EdgeInsets.only(left: 20),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            size: 12,
          ),
          onTap: () {
            BlogNews.showList(
                context: context, cateId: category.id, cateName: category.name);
          },
        ),
      );
    }
    for (var i in children) {
      list.add(
        ListTile(
          leading: Padding(
            child: Text(i.name),
            padding: EdgeInsets.only(left: 20),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            size: 12,
          ),
          onTap: () {
            BlogNews.showList(context: context, cateId: i.id, cateName: i.name);
          },
        ),
      );
    }
    return list;
  }

  List showCategories() {
    final categories = Provider.of<CategoryModel>(context).categories;
    List<Widget> widgets = [];

    if (categories != null) {
      var list = categories.where((item) => item.parent == 0).toList();
      for (var index in list) {
        widgets.add(
          ExpansionTile(
            title: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: Text(
                index.name.toUpperCase(),
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
            children: getChildren(categories, index),
          ),
        );
      }
    }
    return widgets;
  }

  bool checkIsAdmin() {
   return loggedInUser.email == config.adminEmail;
  }

  @override
  void initState() {
    getCurrentUser();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool loggedIn = Provider.of<UserModel>(context).loggedIn;

    return Container(
        color: Theme.of(context).backgroundColor,
        child: DefaultTabController(
          length: 4,
          child: Scaffold(

            backgroundColor: Theme.of(context).backgroundColor,
            resizeToAvoidBottomPadding: false,
            key: _scaffoldKey,
            body: _tabView[selectedIndex],
            bottomNavigationBar: CupertinoTabBar(

              items: [
                BottomNavigationBarItem(
                  icon: ActiveIcon(title: "Home",icon: Icons.home,active: selectedIndex == 0),
                ),
                BottomNavigationBarItem(
                  icon: ActiveIcon(title: "Cerca",icon: Icons.search,active: selectedIndex == 1),
                ),
                BottomNavigationBarItem(
                  icon: ActiveIcon(title: "Preferiti",icon: Icons.favorite,active: selectedIndex == 2),
                ),
                BottomNavigationBarItem(
                  icon: ActiveIcon(title: "Profilo",icon: Icons.person,active: selectedIndex == 3),
                ),

              ],
              onTap: (index) {
                setState(() {
                  selectedIndex = index;
                });
              },
              currentIndex: selectedIndex,
              backgroundColor: Colors.white,
            )
          ),
        ));
  }
}





