import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActiveIcon extends StatefulWidget {
  IconData icon;
  String title;
  bool active = false;

  ActiveIcon({
    this.icon,
    this.title,this.active : false}):
      assert(icon != null),
      assert(title != null);

  @override
  State<StatefulWidget> createState() {
    return _ActiveIconState();
  }
}



class _ActiveIconState extends State<ActiveIcon> with TickerProviderStateMixin{


  @override
  Widget build(BuildContext context) {

    bool active = widget.active;

    return AnimatedContainer(
      curve: Curves.ease,
      duration: Duration(milliseconds: 1000),
      child: Container(
          height: 30,
          decoration: BoxDecoration(
              color: active ? Theme.of(context).primaryColor : Colors.transparent,
              borderRadius: BorderRadius.circular(30)
          ),
          child: Padding(
            padding: const EdgeInsets.only(left:8.0,right: 8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(widget.icon,color: active ? Colors.white : Theme.of(context).accentColor,size: 24),
                if(active)
                SizedBox(width: 3,),Text(widget.title,style: TextStyle(color: Colors.white),textScaleFactor: 1.3,)

              ],
            ),
          ),
      ),
    );
  }

}