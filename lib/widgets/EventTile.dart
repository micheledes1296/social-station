
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';

class EventTile extends StatelessWidget {

  var config;
  String title;



  EventTile({this.config,this.title});
  @override
  Widget build(BuildContext context) {
    //TODO: IMPLEMENT BUILD
    return AspectRatio(
         aspectRatio: 358/83,
         child: ClipRRect(
           borderRadius: BorderRadius.all(Radius.circular(22)),
           child: Container(
             decoration: BoxDecoration(
               gradient: LinearGradient(
                 colors: [Color.fromRGBO(config["color1"]["r"].toInt(), config["color1"]["g"].toInt(), config["color1"]["b"].toInt(), config["color1"]["o"].toDouble()),Color.fromRGBO(config["color2"]["r"].toInt(), config["color2"]["g"].toInt(), config["color2"]["b"].toInt(), config["color2"]["o"].toDouble())],
               ),
             ),
             child: Padding(
               padding: const EdgeInsets.only(top: 30.3, bottom: 25.5),
               child:
               Center(
                 child: Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: FittedBox(fit: BoxFit.contain,child: Center(child: Text(title.toUpperCase(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),maxLines: 2,textAlign: TextAlign.center,textScaleFactor: 1.7,))),
                 ),
               ),
             ),
           ),

         ),
    );
  }

}

