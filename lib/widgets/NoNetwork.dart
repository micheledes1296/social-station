import 'package:flutter/cupertino.dart';

class NoNetworkWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: Center(child: Image( image: AssetImage("assets/images/sad.png"))),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FittedBox(fit:BoxFit.fitWidth,child: Text("Nessuna connessione a Internet!",style: TextStyle(fontWeight: FontWeight.bold),textScaleFactor: 3,maxLines: 1,)),
            ),
            Padding(
              padding: const EdgeInsets.only(left:8.0,right: 8,top:8,bottom: 50),
              child: FittedBox(fit:BoxFit.fitWidth,child: Text("Prova ad aggiornare il contenuto con una connessione internet attiva",style: TextStyle(fontWeight: FontWeight.normal),textScaleFactor: 1)),
            )

          ],
        )
    );
  }
}