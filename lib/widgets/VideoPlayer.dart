import 'dart:async';

import 'package:SocialStation/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:video_player/video_player.dart' as VP;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:async/src/async_memoizer.dart';

class VideoPlayer extends StatefulWidget {
  String videoURL;
  VideoPlayer({this.videoURL});

  _VideoPlayerState createState() => _VideoPlayerState();
}


class _VideoPlayerState extends State<VideoPlayer> {

  VP.VideoPlayerController _playerController;
  Function listener;
  int turns;
  var subscription;
  var urlManager;
  Future<YTVideo> videoData;
  final _memoizer = AsyncMemoizer<YTVideo>() ;
  Timer timer;
  bool showHUD = false;

  @override
  void initState() {
    turns = 0;

    videoData = _memoizer.runOnce( () => _YouTubeURLManager(url:widget.videoURL).ytVideoData);
    timer = Timer(Duration(seconds:5), () {
      setState(() {
        showHUD = false;
      });
    });

    listener = () {setState(() {});};

    super.initState();
  }

  void createVideo() async {
    if(_playerController == null){
      _playerController = VP.VideoPlayerController.network((await videoData).url)
        ..addListener(listener)
        ..setVolume(1.0)
        ..initialize();
    }
  }

  @override
  void deactivate() {
    _playerController.setVolume(0);
    _playerController.removeListener(listener);
    super.deactivate();
  }


  @override
  Widget build(BuildContext context) {
    //TODO: IMPLEMENT BUILD
    return RotatedBox(
      quarterTurns: turns,
      child: FutureBuilder(
        future: videoData,
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.waiting || snapshot.connectionState == ConnectionState.none)
            return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 16/9,
                    child: Container(
                      color: Colors.grey,
                      child: Center(
                        child: CircularProgressIndicator()
                      ),
                    ),
                  ),
                  Text("loading...",style: TextStyle(fontSize:20,fontWeight: FontWeight.w800)),
                  Text("loading...",style: TextStyle(fontSize:18,color: Theme.of(context).primaryColorLight)),
                ],
              );

          if(snapshot.hasData) {
            return  Column(
                children: <Widget>[
                  AspectRatio(
                      aspectRatio: 16 / 9,
                      child: Stack(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              child: Container(
                                child: (_playerController != null ? VP.VideoPlayer(
                                    _playerController) : Container()),
                              ),
                              onLongPress: () {
                                //TODO: Apri su youtube
                              },
                              onTap: () {
                                setState(() {
                                  showHUD = true;
                                });

                              },
                            ),
                          ),
                          if (showHUD == true)
                          Positioned(
                            bottom: 8,
                            right: 8,
                            child: IconButton(
                              icon: turns == 0 ? Icon(Icons.fullscreen) : Icon(Icons
                                  .fullscreen_exit),
                              onPressed: () {
                                setState(() {
                                  turns = 1;
                                });
                              },
                            ),
                          ),
                          if (showHUD == true)
                          Center(
                            child: IconButton(
                              onPressed:  () {
                                  setState(() {
                                    _playerController.value.isPlaying ? _playerController.pause() : _playerController.play();
                                  });
                                },
                              icon: _playerController.value.isPlaying ?  Icon(Icons.play_arrow) : Icon(Icons.pause),
                              iconSize: 40,
                            ),
                          )
                        ],
                      )
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text((snapshot.data as YTVideo).title, style: TextStyle(fontWeight: FontWeight.w800, fontSize: 20)),
                      Text((snapshot.data as YTVideo).timeAgo, style: TextStyle(color:Theme.of(context).primaryColorLight)),
                    ]
                  ),
                  SizedBox(height: 10),
                  Text((snapshot.data as YTVideo).description, style: TextStyle(color:Theme.of(context).primaryColorLight)),

                ],
              );
          }else {
            return Container(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Icon(Icons.error_outline),
                  ),
                  Text(
                    "There was an error: ${snapshot.error.toString()}"
                  )
                ],
              ),
            );
          }
        }
      ),
    );
  }

}


class _YouTubeURLManager  {

    String url;

  _YouTubeURLManager({this.url});

  Future<YTVideo> get ytVideoData async =>  await getVideoData(this.url);

  Future<YTVideo> getVideoData(String url) async {

    YTVideo video = YTVideo();

    final URLresponse =  await http.get("https://you-link.herokuapp.com/?url=${url}");

    if (URLresponse.statusCode == 200) {
      // return decoded json
      video.url = await getRightURL(json.decode(URLresponse.body));
    } else {
      throw Exception("Failed to load video URL");
    }

    final videoID = YoutubePlayer.convertUrlToId(url);
    

    final metadataResponse = await http.get("https://www.googleapis.com/youtube/v3/videos?id=${videoID}&key=${kYouTubeAPI}&part=snippet,contentDetails&fields=items(id,snippet(title,description,thumbnails(default(url)),publishedAt),contentDetails(duration))");
    final jsonMetadata = json.decode(metadataResponse.body);
    video.description = jsonMetadata['items'].first['snippet']['description'];
    video.title = jsonMetadata['items'].first['snippet']['title'];
    video.id = videoID;
    video.thumbnailURL = jsonMetadata['items'].first['snippet']['thumbnails']['default']['url'];
    video.duration = parseStringDuration(jsonMetadata['items'].first['contentDetails']['duration']);
    video.timeAgo = howLongSince(jsonMetadata['items'].first['snippet']['publishetAt']);
    return video;

  }


  Duration parseStringDuration(String sduration){
    int hours;
    int minutes;
    int seconds;
    
    String cleanString = sduration.replaceAll("PT", "");

    var splittedString = cleanString.split("H");

    if (splittedString.first != "") {
      hours = int.parse(splittedString.first);
      cleanString = splittedString[1] ?? "";
    }

    splittedString = cleanString.split("M");

    if (splittedString.first != "") {
      minutes = int.parse(splittedString.first);
      cleanString = splittedString[1] ??  "";
    }

    splittedString = cleanString.split("S");

    if (splittedString.first != "") {
      seconds = int.parse(splittedString.first);
    }
    return Duration(hours: hours ?? 0,minutes: minutes ?? 0, seconds: seconds ?? 0);
  }


  String howLongSince(String publishingDate) {
   if (publishingDate.contains("T")){
    publishingDate = publishingDate.replaceRange(publishingDate.indexOf("T"), publishingDate.length-1,"");
   }
   var pubDate = DateTime.parse(publishingDate);

   var howLongSince = DateTime.now().difference(pubDate);

   if (howLongSince.inDays < 31) {
     return "${howLongSince.inDays} ${ howLongSince.inDays == 1 ? 'giorno' : 'giorni' } fa";
   } else if (howLongSince.inDays < 365) {
     return "${(howLongSince.inDays / 30).round()} ${ (howLongSince.inDays/30).round() == 1 ? 'mese' : 'mesi' } fa";
   } else {
     return "${(howLongSince.inDays / 365). round()} ${ (howLongSince.inDays/365).round() == 1 ? 'anno' : 'anni' } fa";
   }

  }


  Future<String> getRightURL(List<Map<String,dynamic>> json) async  {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return (json[0]['url'] as String).replaceAll("&c=WEB", "");
    }else {
      return (json[1]['url'] as String).replaceAll("&c=WEB", "");
    }
  }

}


class YTVideo {
  String url;
  String title;
  Duration duration;
  String id;
  String description;
  String thumbnailURL;
  String timeAgo;

  YTVideo({this.url,this.title,this.duration,this.id,this.description,this.thumbnailURL,this.timeAgo});

  factory YTVideo.fromJson(Map<String,dynamic> json) {
    return YTVideo(
      url: json['url'],
      title: json['title'],
      duration: json['duration'],
      id: json['id'],
      description: json['description'],
      thumbnailURL: json['thumbnailURL'],
      timeAgo: json['timeAgo']
    );
  }
}