import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';


class YouTubePlayerTile extends StatefulWidget {
  final videoID;
  YouTubePlayerTile({this.videoID});
  _YouTubePlayerTileState createState() => _YouTubePlayerTileState();
}


class _YouTubePlayerTileState extends State<YouTubePlayerTile> {
  YoutubePlayerController _controller;
  var _isPlayerReady = false;
  var _videoMetaData;

  @override
  void initState() {
    _controller = YoutubePlayerController(
      initialVideoId: widget.videoID,

      flags: YoutubePlayerFlags(
        mute: true,
        autoPlay: false,
        forceHideAnnotation: false,
        disableDragSeek: true
      ),
    );
    print("metadata:  ${_controller.metadata}");
    _videoMetaData = YoutubeMetaData();
   _controller.addListener(listener);
    super.initState();
  }
  void listener() {
    print("in listener");
    setState(() {
      _videoMetaData = _controller.metadata;
    });
    if (_isPlayerReady ) {
      print("in if");
      setState(() {
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: screenWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            child: YoutubePlayer(
                  width: screenWidth,
                  controller: _controller,
                  showVideoProgressIndicator: true,
                  progressIndicatorColor: Colors.orange,
                  progressColors: ProgressBarColors(
                    playedColor: Colors.orange,
                    handleColor: Colors.orange
                  ),

              onReady: () {
                    setState(() {
                      _isPlayerReady = true;
                      _videoMetaData = _controller.metadata;
                    });
              },

            )
          ),

          SizedBox(height: 10),

          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  _videoMetaData.title,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w800,
                    color: Theme.of(context).accentColor,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(_videoMetaData.author,
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ],
            ),
          )

        ],
      ),

    );
  }

}


class YouTubePlayerList extends StatelessWidget {
  final config;
  YouTubePlayerList({this.config});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, top: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10,bottom: 10),
            child: Text(
              "YouTube",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(
            height:280,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  for(int i = 0 ; i< config["VideoIDs"].length;i++)
                    YouTubePlayerTile(videoID:config["VideoIDs"][i]["id"])
                ],
              ),
            )
          )


        ],
      ),
    );
  }
}