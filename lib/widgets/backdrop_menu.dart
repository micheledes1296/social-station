import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../common/constants.dart';
import '../generated/i18n.dart';
import '../models/app.dart';
import '../models/category.dart';
import 'tree_view.dart';

class BackdropMenu extends StatefulWidget {
  final Function onFilter;

  const BackdropMenu({
    Key key,
    this.onFilter,
  }) : super(key: key);

  @override
  _BackdropMenuState createState() => _BackdropMenuState();
}

class _BackdropMenuState extends State<BackdropMenu>
    with SingleTickerProviderStateMixin, AfterLayoutMixin<BackdropMenu> {
  double mixPrice = 0.0;
  double maxPrice = kMaxPriceFilter / 2;
  int categoryId = -1;

  @override
  void afterFirstLayout(BuildContext context) {
    Provider.of<CategoryModel>(context).getCategories();
  }

  @override
  Widget build(BuildContext context) {
    final category = Provider.of<CategoryModel>(context);
    final selectLayout = Provider.of<AppModel>(context).productListLayout;

    return ListenableProvider.value (
        value: category,
        child: Consumer<CategoryModel>(builder: (context, value, child) {
          if (value.isLoading) {
            return Center(child: kLoadingWidget(context));
          }

          if (value.message != null) {
            return Center(
              child: Text(value.message),
            );
          }

          if (value.categories != null) {
            final categories = value.categories.where((item) => item.parent == 0).toList();

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Text(
                    S.of(context).layout,
                    style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.w700,
                      color: Colors.white70,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    SizedBox(
                      width: 10.0,
                    ),
                    for (var item in kProductListLayout)
                      GestureDetector(
                        onTap: () =>
                            Provider.of<AppModel>(context).updateProductListLayout(item['layout']),
                        child: Container(
                          width: 40,
                          height: 40,
                          margin: EdgeInsets.all(10.0),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Image.asset(
                              item['image'],
                              color: selectLayout == item['layout']
                                  ? Colors.white
                                  : Colors.black.withOpacity(0.2),
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: selectLayout == item['layout']
                                  ? Colors.black.withOpacity(0.15)
                                  : Colors.black.withOpacity(0.05),
                              borderRadius: BorderRadius.circular(9.0)),
                        ),
                      )
                  ],
                ),
              ],
            );
          }
          return Container();
        }));
  }

  bool hasChildren(categories, id) {
    return categories.where((o) => o.parent == id).toList().length > 0;
  }

  List<Category> getSubCategories(categories, id) {
    return categories.where((o) => o.parent == id).toList();
  }
}

class CategoryItem extends StatelessWidget {
  final Category category;
  final bool isLast;
  final bool isSelected;
  final bool hasChild;
  final Function onTap;

  CategoryItem(this.category,
      {this.isLast = false, this.isSelected = true, this.hasChild = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return null;
  }
}
