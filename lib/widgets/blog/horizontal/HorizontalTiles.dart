import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:SocialStation/services/wordpress.dart';
import 'package:SocialStation/models/blog_news.dart';
import 'package:async/src/async_memoizer.dart';
import 'package:provider/provider.dart';
import 'package:SocialStation/models/app.dart';


class HorizontalTiles extends StatefulWidget {

  final config;
  HorizontalTiles({this.config});

  _HorizontalTilesState createState() => _HorizontalTilesState();
}


class _HorizontalTilesState extends State<HorizontalTiles> {
  final WordPress _service = WordPress();
  Future<List<BlogNews>> _getBlogsLayout;

  final _memoizer = AsyncMemoizer<List<BlogNews>>();

  @override
  void initState() {
    // only create the future once
    new Future.delayed(Duration.zero, () {
      setState(() {
        _getBlogsLayout = getBlogLayout(context);
      });
    });
    super.initState();
  }

  Future<List<BlogNews>> getBlogLayout(context) => _memoizer.runOnce(
        () => _service.fetchBlogLayout(
        config: widget.config, lang: Provider.of<AppModel>(context).locale),
  );


  _launchURL(String url) async {

    if (await canLaunch(url)) {
      await launch(url,forceSafariVC: false,forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }


  @override
  Widget build(BuildContext context) {
    List items = widget.config['elements'];
    //TODO: IMPLEMENT BUILD
    return FutureBuilder(
      future: _getBlogsLayout,
      builder: (context, snapshot) {
        switch(snapshot.connectionState){
          case ConnectionState.none:
          case ConnectionState.waiting:
          case ConnectionState.active:
           return Container();
          case ConnectionState.done:
            if(snapshot.hasError)
              return Container();
            else
              return  Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                for ( int i =0; i< items.length; i++)
                  Flexible(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        _launchURL(items[i]["url"]);
                      },
                      child: Container(
                        height: 52,
                        child: Center(
                          child:Text(
                            items[i]["text"] ?? "",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold
                            ),
                          ),

                        ),
                        decoration: BoxDecoration(
                            color: Color(int.parse(items[i]["color"])) ?? Colors.black,
                            border: Border.all(width: 5,color: Colors.white),
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                      ),
                    ),
                  )
              ],
            );
        }
        return Container();
      }
    );
  }

}