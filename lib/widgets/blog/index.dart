import 'package:SocialStation/widgets/YouTubePlayer.dart';
import 'package:SocialStation/widgets/blog/horizontal/HorizontalTiles.dart';
import 'package:flutter/material.dart';

import 'banner/banner_animate_items.dart';
import 'banner/banner_group_items.dart';
import 'banner/banner_slider_items.dart';
import 'category/category_icon_items.dart';
import 'category/category_image_items.dart';
import 'header/header_text.dart';
import 'logo.dart';
import 'vertical/vertical_layout.dart';
import 'horizontal/horizontal_list_items.dart';
import 'horizontal/slider_list.dart';
import 'horizontal/slider_item.dart';
import 'horizontal/blog_list_layout.dart';

class HomeLayout extends StatefulWidget {
  final configs;

  HomeLayout({this.configs});

  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> with AutomaticKeepAliveClientMixin {

  bool rebuild = true;


  @override
  bool get wantKeepAlive => true;

  /// convert the JSON to list of horizontal widgets
  Widget jsonWidget(config) {
    switch (config["layout"]) {
      case "logo":
        return Logo();

      case"YouTubeList":
        return YouTubePlayerList(config: config);

      case 'header_text':
        return HeaderText(config: config);

      case  'horizontal tiles' :
        return HorizontalTiles(config: config);

      case "category":
        return (config['type'] == 'image')
            ? CategoryImages(config: config)
            : CategoryIcons(config: config);

      case "bannerAnimated":
        return BannerAnimated(config: config);

      case "bannerImage":
        return config['isSlider'] == true
            ? BannerSliderItems(config: config)
            : BannerGroupItems(config: config);
      case 'largeCardHorizontalListItems':
        return LargeCardHorizontalListItems(
          config: config,
        );
      case "sliderList":
        return HorizontalSliderList(
          config: config,
        );
      case "sliderItem":
        return SliderItem(
          config: config,
        );

      default:
        return BlogListLayout(config: config);
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (widget.configs == null) return ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.width -10,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image( image: AssetImage("assets/images/sad.png")),
            Text("Nessuna connessione a Internet!",style: TextStyle(fontWeight: FontWeight.bold),textScaleFactor: 3),
            Text("Prova ad aggiornare il contenuto con una connessione internet attiva",style: TextStyle(fontWeight: FontWeight.normal),textScaleFactor: 1)

          ],
        )
    );

    return RefreshIndicator(
      onRefresh: () => Future.delayed(
        Duration(milliseconds: 1000),
          () {
            setState(() {
              rebuild = false;
              rebuild = true;
            });
          }
      ),
      child: rebuild ? SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: <Widget>[
            for (var config in widget.configs["HorizonLayout"])
              jsonWidget(
                config,
              ),
          ],
        ),
      ):Container(),
    );
  }
}
