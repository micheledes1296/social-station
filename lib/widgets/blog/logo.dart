import 'package:flutter/material.dart';

import '../../common/constants.dart';

class Logo extends StatelessWidget {
  double size;
  Logo({this.size});
  @override
  Widget build(BuildContext context) {
    return Container(
      child:
          Center(
            child: Row(children:[Text("SOCIAL", style: TextStyle(fontWeight: FontWeight.normal, color: Colors.black,fontSize: this.size ?? 16)), Text("STATION", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.orange,fontSize: this.size ?? 16)),Text(".IT", style: TextStyle(fontWeight: FontWeight.normal, color: Colors.black,fontSize: this.size ?? 16))]),
          ),
    );
  }
}
