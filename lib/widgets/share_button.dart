import 'package:flutter/material.dart';
import 'package:share/share.dart';

class ShareButton extends StatelessWidget {
  final String blogSlug;
  ShareButton({this.blogSlug});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
//
          Share.share(
              'Hey dai un\'occhiata a questo articolo!!! https://www.socialstation.it/news/$blogSlug');
        },
        child: Container(
          margin: EdgeInsets.all(12.0),
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Icon(
            Icons.share,
            size: 18.0,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
